import { Component, EventEmitter, Input, Output } from '@angular/core';
import { LoginService } from './login.service';

@Component({
  selector: 'lib-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css'] 
})
export class LoginComponent {

  user: string;
  password: string;
  @Output() public token = new EventEmitter<string>();

  constructor(public loginService: LoginService) { }

  login() {
    const user = {usuario: this.user, contraseña: this.password};
    this.loginService.login(user).subscribe( data => {
      this.loginService.setToken(data.token);
      this.sendToken(data.token);
    });
  }

  sendToken(token: string) {
    this.token.emit(token);
  }

}
